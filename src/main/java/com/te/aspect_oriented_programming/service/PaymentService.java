package com.te.aspect_oriented_programming.service;

public interface PaymentService {

	public void makePayment();
}
