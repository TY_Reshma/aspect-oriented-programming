package com.te.aspect_oriented_programming.service;

public class PaymentServiceImpl implements PaymentService {

	@Override
	public void makePayment() {
		System.out.println("Amount Credited");
		
		System.out.println("Amount Debited");
	}

}
