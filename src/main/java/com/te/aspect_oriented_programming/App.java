package com.te.aspect_oriented_programming;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.te.aspect_oriented_programming.service.PaymentService;

public class App {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Configuration.xml");
		PaymentService paymentObject = context.getBean("Payment", PaymentService.class);
		paymentObject.makePayment();
	}
}
